from sqlobject import *


from base import SQLAbyeObject

class Deposit(SQLAbyeObject):

    @classmethod
    def create_table(cls):
        if not cls.tableExists():
            cls.createTable()
    '''
    Deposit Object
    '''
    ## properties
    name = StringCol(length=255, default=None)
    bank = ForeignKey("Bank")
    interest_rate = DecimalCol(size=10,precision=5)
    min_amount = IntCol()
