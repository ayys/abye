from sqlobject import *
from base import SQLAbyeObject

class Branch(SQLAbyeObject):
    '''
    Branch Object
    '''

    location = StringCol()
    bank = ForeignKey("Bank")

    @classmethod
    def create_table(cls):
        if not cls.tableExists():
            cls.createTable()

