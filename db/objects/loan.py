
from sqlobject import *
from sqlobject.sqlbuilder import LIKE

from base import SQLAbyeObject

class Loan(SQLAbyeObject):

    @classmethod
    def create_table(cls):
        if not cls.tableExists():
            cls.createTable()
    '''
    Loan Object
    '''
    ## properties
    name = StringCol(length=255, default=None)
    bank = ForeignKey("Bank")
    interest_rate = DecimalCol(size=10,precision=5)
    year = IntCol()
    category = StringCol(default=None)
    max_amount = IntCol()

    @classmethod
    def getByName(cls, name):
        name.replace("%", "");
        like_query = LIKE(cls.q.name, "%{}%".format(name));
        query_result = cls.select(like_query)
