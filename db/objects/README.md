                                        Bank _________|
                                         |            |name|
                                         |            |branches|
                                         |            |loans|
                                         |            |deposits|
                  |______________________ ________________________|
                  |name|                 |                        |name|
                  |bank|                 |                        |bank|
                  |interest_rate|        |bank|                   |interest_rate|
                  |max_amount|           |location|               |min_amount|
                Loans                  Branch                  Deposit
                _____                  ______                  _______

## Methods

### Bank

0. __init_database__ - **RUN THIS FUNCTION BEFORE DOING ANYTHING WITH THE
   DATABASE** initializes the database(SQLite) and creates the required tables if they
   have not yet been created


**args** - string : Name of database file (SQLite)



1. __create_table__ - checks if a table for this class has been created.
                  if there is no table, creates a new table.

**args** - NO ARGUMENTS


2. __add_branch__ - adds a new branch to the bank.
                

**args** - __location__ : location of the branch


3. __remove_branch__ - takes a branch object and removes it


**args** -  Branch Object

4. __add_loan__ - Adds a new loan object to the bank


**args** - name (name of loan), interest_rate, max_amount

5. __remove_loan__ - takes a Loan object and removes it


**args** -  Loan Object


6. __add_deposit__ - Adds a new Deposit object to the bank


**args** - name (name of deposit), interest_rate, max_amount

7. __remove_deposit__ - takes a Deposit object and removes it


**args** -  Deposit Object



## Demo Code

```
              from db.objects.bank import Bank
              from db.objects.branches import Branch

              Bank.init_database("database.db")    # initializes the database - do this
                                      # before anything else.
                                      # Note : database.db file must exist

              everest_b = Bank(name = "Everest Bank")   # creates a new bank object
              everest_b.sync()   # adds the object to database

              # creates a branch object. Note that by specifying the bank
              # attribute, the branch is automatically added to everest_b
              branch = Branch(location = "Lazimpat", bank = everest_b);

              # adds the object to database
              branch.sync()

              for branch in everest_b.branches:
                  print branch        # prints the lazimpat branch

              Branch.delete(branch)

              for branch in everest_b.branches:
                  print branch        # lazimpat branch has been removed, so
                                      # nothing is printed
```
