from sqlobject import *

from base import SQLAbyeObject

from branch import Branch
from loan import Loan
from deposit import Deposit

from settings.database import make_database_file


class Bank(SQLAbyeObject):

    @staticmethod
    def init_database(database_name = ":memory:"):
        '''
        initiates the database, and adds all the tables if not yett created
        takes argument - database_name (string) to open
        '''
        if database_name != ":memory:":
            make_database_file(database_name)
        sqlhub.processConnection = connectionForURI(
                'sqlite:///{}'.format(database_name))
        Bank.create_table()
        Branch.create_table()
        Loan.create_table()
        Deposit.create_table()


    '''
    Bank Object
    '''
    ## properties
    name = StringCol(length=255, default=None)
    branches = MultipleJoin("Branch")
    loans = MultipleJoin("Loan")
    deposits = MultipleJoin("Deposit")

    @classmethod
    def create_table(cls):
        if not cls.tableExists():
            cls.createTable()

    def add_branch(self, location):
        '''
        adds a new beanch to the bank object
        takes argument : location (location of the branch)
        '''
        if location.__class__ != str:
            raise TypeError("branch location is not of type str")

        branch = Branch(location=location, bank = self)
        branch.sync()


    def remove_branch(self, branch):
        '''
        takes a branch object and removes it
        takes argument : Branch object
        '''
        if branch.__class__ == Branch:
            Branch.delete(branch.id)


    def add_loan(self, name, interest_rate, max_amount, year, category):
        '''
        adds a new loan object to the bank
        takes argument : name (name of loan), interest_rate, max_amount
        '''
        loan = Loan(
                name=name,
                interest_rate = interest_rate,
                max_amount = max_amount,
                year = year,
                category=category,
                bank = self)
        loan.sync()


    def remove_loan(self, loan):
        '''
        takes a branch object and removes it
        takes argument : Loan object
        '''
        if loan.__class__ == Loan:
            Loan.delete(loan.id)

    def add_deposit(self, interest_rate, min_amount):
        '''
        adds a new deposit to the bank
        takes argument : name(name of deposit), interest_rate, min_amount
        '''
        deposit = Deposit(bank = self,
                name = name,
                interest_rate = interest_rate,
                min_amount = min_amount)


    def remove_deposit(self, deposit):
        '''
        takes a branch object and removes it
        takes argument : Deposit object
        '''
        if deposit.__class__ == Deposit:
            Deposit.delete(deposit.id)


    '''
    search function
    '''
