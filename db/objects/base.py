from sqlobject import SQLObject, SQLObjectNotFound


class SQLAbyeObject(SQLObject):
    @classmethod
    def get_all(cls):
        return list(cls.select())

    class sqlmeta:
        lazyUpdate = False

    @classmethod
    def getBy(cls, field, like_str):
        qobj = cls.q.__getattr__(field)
        return [_ for _ in cls.select(qobj.contains(like_str))]


