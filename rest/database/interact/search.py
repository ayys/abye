
from db.objects.entities import *

from settings.database import get_database_name


Bank.init_database(get_database_name())

def search_banks(field, string):

    '''
    search banks where
    field - name of attribute
    string - search string to be used
    example : 
    search_bank("name", "nabil")
    uses teh LIKE sql query, so only partial names work as well
    '''
    banks = Bank.getBy(field, string)
    out = list()
    for bank in banks:
        data = {
                    "name": bank.name,
                    "branches": [x.location for x in bank.branches],
                    "loans": [
                        {
                            "name": _.name,
                            "interest_rate": _.interest_rate,
                            "year": _.year,
                            "category": _.category,
                            "max_amount": _.max_amount
                            } for _ in bank.loans],
                    "deposits":[
                        {
                            "name": _.name,
                            "interest_rate": _.interest_rate,
                            "min_amount": _.min_amount
                            } for _ in bank.deposits],
                    }
        out.append(data)
    return out


def search_loans(field, string):
    '''
    search loans where
    field - name of attribute
    string - search string to be used
    example : 
    search_loans("name", "nabil")
    uses the LIKE sql query, so only partial names work as well
    '''
    loans = Loan.getBy(field, string)
    out = list()
    for _ in loans:
        data = {
                "name": _.name,
                "interest_rate": _.interest_rate,
                "year": _.year,
                "category": _.category,
                "max_amount": _.max_amount,
                "bank_name" : _.bank.name
                }
        out.append(data)
    return out


