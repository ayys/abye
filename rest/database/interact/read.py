from db.objects.bank import Bank

from settings.database import get_database_name


Bank.init_database(get_database_name())

def read_all_banks():
    banks = Bank.get_all()
    out = list()
    for bank in banks:
        data = {
                    "name": bank.name,
                    "branches": [x.location for x in bank.branches],
                    "loans": [
                        {
                            "name": _.name,
                            "interest_rate": _.interest_rate,
                            "max_amount": _.max_amount
                            } for _ in bank.loans],
                    "deposits":[
                        {
                            "name": _.name,
                            "interest_rate": _.interest_rate,
                            "min_amount": _.min_amount
                            } for _ in bank.deposits],
                    }
        out.append(data)
    return out
