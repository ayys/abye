from db.objects.bank import Bank

from settings.database import get_database_name


Bank.init_database(get_database_name())

def add_bank(name, branches, loans, deposits):
    '''
    adds a bank.
    Arguments : name - name of bank
    branches : list of dictionary objects -----
                    {
                        "location": _______
                    }
    loans : list of dictionary objects -----
                    {
                        "name": _______,
                        "interest_rate": _____,
                        "max_amount": _____
                    }
    deposits : list of dictionary objects -----
                    {
                        "name": _______,
                        "interest_rate": _____,
                        "min_amount": _____
                    }

    '''
    if\
            branches.__class__ is not list or\
            loans.__class__ is not list or\
            deposits.__class__ is not list:
                return 0

    branch_objs = []; deposit_objs = []; loan_objs = []

    bank = Bank(name=name)

    for _ in branches:
        if _.__class__ == dict:
            loc = _.get("location", "")
            bank.add_branch(location=loc)

    for _ in loans:
        if _.__class__ == dict:
            name = _.get("name", "")
            interest_rate = _.get("interest_rate", 0)
            max_amount = _.get("max_amount", 0)
            bank.add_loan(
                    name=name,
                    interest_rate=interest_rate,
                    max_amount=max_amount
                )


    for _ in deposits:
        if _.__class__ == dict:
            name = _.get("name", "")
            interest_rate = _.get("interest_rate", 0)
            min_amount = _.get("min_amount", 0)
            bank.add_deposit(
                    name=name,
                    interest_rate=interest_rate,
                    min_amount=min_amount
                )

    bank.sync()
    return bank
