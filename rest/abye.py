#!/usr/bin/python

from flask import Flask,request,jsonify, render_template
from flask_cors import CORS,cross_origin

from requests.banks import get_bank,get_all_banks
from requests.loan import get_loan
from requests.deposit import get_deposit


from db.objects.bank import Bank

app = Flask(__name__)  # flask object
cors = CORS(app)  # allow cross domain access
app.config['CORS_HEADERS'] = 'Content-Type'



# routing index to welcome page
@app.route("/")
@cross_origin()
def index():
    return render_template("welcome.html")

# return all banks detail
@app.route("/banks/", methods=["GET"])
@cross_origin()
def all_banks():
    return jsonify(get_all_banks())

# return about a specific bank detail
@app.route("/banks/<string:bank_name>/", methods=["GET"])
@cross_origin()
def get_bank_by_name(bank_name):
    return jsonify(get_bank(bank_name))

# returns about all loans
@app.route("/loan/", methods=["GET"])
@cross_origin()
def loan():
    return jsonify(get_loan())


# returns about a type of loan
@app.route('/loan/<string:loan_type>/', methods=["GET"])
@cross_origin()
def loan_type(loan_type):
    return  jsonify(get_loan(loan_type))


# returns about all deposit
@app.route('/deposit/', methods=["GET"])
@cross_origin()
def deposit():
    return jsonify(get_deposit())


# returns about a type of deposit
@app.route('/deposit/<string:deposit_type>/', methods=["GET"])
@cross_origin()
def deposit_type(deposit_type):
    return jsonify(get_deposit(deposit_type))

if __name__ == "__main__":
    app.run(debug=True)
