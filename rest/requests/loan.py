from rest.database.interact.read import read_all_banks
from rest.database.interact.search import search_loans

banks = read_all_banks()


def get_loan(type=None):
    """
    It handles get request for loans

    :param type:[optional] type of loan::
       if type is specified:
          :returns: dictionary of type of loan
       else
          :returns: dictionary of all loans
    """

    loans = []
    # generate list of banks with their branch and loans
    for bank in banks:
        loan = {"loans": bank["loans"], "branches": bank["branches"], "name": bank["name"]}
        loans.append(loan)

    data = {}
    if type:  # if type is specified

        data = {
            "status": "valid",
            "data": []
        }
        if search_loans("name", type).__len__() == 0:
            return {"status": "invalid", "message": "Search empty,loan type not found"}
        else:
            data["data"] = (search_loans("name", type))
            return data

    else:  # if type is not specified
        data = {
            'status': 'valid',
            'data': loans
        }
        return data