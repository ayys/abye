from rest.database.interact.read import read_all_banks
from rest.database.interact.search import search_banks


def get_all_banks():
    """
    Gives everything in a result
    
    :return: dictionary of all records 
    """

    data = {
        'status': 'valid',
        'data': read_all_banks()
    }
    return data


def get_bank(bank_name):
    """
    Returns data of given bank name
    :return: dictionary of record of given bank
    """
    data = {
        "status": "valid",
        "data": []
    }
    if search_banks("name", bank_name).__len__() == 0:
        return {"status": "invalid", "message": "Search empty, Bank not found"}
    else:
        data["data"] = (search_banks("name", bank_name))
        return data