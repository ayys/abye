from rest.database.interact.read import read_all_banks


def get_deposit(type=None):
    """
    It handles get request for deposit

    :param type:[optional] type of deposit ::
       if type is specified:
          :returns: dictionary of type of deposit
       else
          :returns: dictionary of all deposits
    """

    banks = read_all_banks()
    deposits = []
    # generate list of deposits
    for bank in banks:
        deposit = {"branches": bank["branches"], "name": bank["name"]}
        if not bank["deposits"] == []:
            deposit["deposits"] = bank["deposits"]
            deposits.append(deposit)

    data = {}
    if type:  # if type is specified
        data = {
            "status": "valid",
            "data": []
        }
        for deposit in deposits:
            d = {
                "branches": bank["branches"],
                "name": bank["name"],
                "deposits": []
            }
            for l in deposit["deposit"]:
                # if type is found then append loan type to d and append d to data
                if l["name"] == type:
                    d["loans"].append(l)
                    data["data"].append(d)
        if data["data"] == []:
            return {"status": "invalid", "message": "Search empty,deposit type not found"}
        else:
            return data

    else:  # if type is not specified
        data = {
            "status": "valid",
            "data": deposits
        }
        if data["data"] == []:
            return {"status": "invalid", "message": "Search empty"}
        else:
            return data
