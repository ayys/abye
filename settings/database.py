import os
import sys

DATABASE_NAME = "database.db" # database name 

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

sys.path.append(BASE_DIR)

def make_database_file(file_name):
    '''
    if database file does not exist, create the database file
    '''
    if not os.path.exists(file_name):
        f = open(file_name, "w+")
        f.close()


def get_database_name():
    f_name = BASE_DIR + "/" + DATABASE_NAME
    # f_name = f_name.replace("/", "\/")
    return f_name
